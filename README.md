Welcome to the Scaling and Healing VNF Workloads with ESC Lab. 

Your proctor today will be Snezana Mitrovic (snmitrov@cisco.com)


You will need following:

1. PC with internet connectivity and Anyconnect Installed
2. Lab Guide (part of this repository)
3. Scaling config file (part of this repository)


We have 5 slots available today:

VPN:  `dcloud-rtp-anyconnect.cisco.com`


| POD Id | Username   | Password  |
| ------ |----------- | ----------|
| POD-1  | v1252user1 | 7210a5    |
| POD-2  | v376user1  | 16a318    |
| POD-3  | v737user1  | 71b73d    |
| POD-4  | v1076user1 | 75e2ef    |
| POD-5  | v2605user1 | e83fb5    |